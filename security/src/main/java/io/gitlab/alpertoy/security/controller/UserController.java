package io.gitlab.alpertoy.security.controller;

import io.gitlab.alpertoy.security.dto.UserDTO;
import io.gitlab.alpertoy.security.entity.User;
import io.gitlab.alpertoy.security.exception.UserNotFoundException;
import io.gitlab.alpertoy.security.repository.UserRepository;
import io.gitlab.alpertoy.security.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * User: alpertoy
 * Date: 8.02.2021
 * Time: 23:22
 */

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserRepository userRepository;
    private final NotificationService notificationService;

    @Autowired
    public UserController(UserRepository userRepository, NotificationService notificationService) {
        this.userRepository = userRepository;
        this.notificationService = notificationService;
    }

    @PostMapping("/register")
    public void register(@RequestBody UserDTO userDTO) {
        User user = new User(userDTO.getUsername(), userDTO.getPassword());
        userRepository.save(user);
        notificationService.sendMessage(userDTO);
    }

    @PostMapping("/validate")
    public String validateTokenAndGetUsername(@RequestHeader("Authorization") String token) {
        return userRepository.findById(token)
                .orElseThrow(UserNotFoundException::new).getUsername();
    }
}
