package io.gitlab.alpertoy.security.repository;

import io.gitlab.alpertoy.security.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * User: alpertoy
 * Date: 8.02.2021
 * Time: 23:31
 */
public interface UserRepository extends JpaRepository<User, String> {
}
