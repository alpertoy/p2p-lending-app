package io.gitlab.alpertoy.security.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * User: alpertoy
 * Date: 8.02.2021
 * Time: 23:07
 */

@Entity
public class User {

    @Id
    private String username;
    private String password;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
