package io.gitlab.alpertoy.security.service;

import com.google.gson.Gson;
import io.gitlab.alpertoy.security.dto.UserDTO;
import io.gitlab.alpertoy.security.entity.User;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * User: alpertoy
 * Date: 10.02.2021
 * Time: 11:46
 */

@Service
public class NotificationService {

    private final RabbitTemplate rabbitTemplate;
    private final static Gson GSON = new Gson();

    @Autowired
    public NotificationService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessage(UserDTO userDTO) {
        userDTO.setPassword(null);
        rabbitTemplate.convertAndSend("userRegisteredTopic", "user.registered", GSON.toJson(userDTO));
    }
}
