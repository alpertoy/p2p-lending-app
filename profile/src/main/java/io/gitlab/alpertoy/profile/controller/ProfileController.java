package io.gitlab.alpertoy.profile.controller;

import io.gitlab.alpertoy.profile.entity.User;
import io.gitlab.alpertoy.profile.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

/**
 * User: alpertoy
 * Date: 9.02.2021
 * Time: 23:45
 */

@RestController
@RequestMapping("/profile")
public class ProfileController {

    private final UserRepository userRepository;

    @Autowired
    public ProfileController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/users")
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    @PostMapping("/new/user")
    public void newUser(@RequestBody User user) {
        user.setRegisteredSince(LocalDate.now());
        userRepository.save(user);
    }

    @PutMapping("/user")
    public void updateUser(@RequestBody User user) {
        userRepository.save(user);
    }
}
