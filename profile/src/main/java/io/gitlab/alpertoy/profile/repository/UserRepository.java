package io.gitlab.alpertoy.profile.repository;

import io.gitlab.alpertoy.profile.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * User: alpertoy
 * Date: 9.02.2021
 * Time: 23:44
 */
public interface UserRepository extends JpaRepository<User, String> {
}
