package io.gitlab.alpertoy.lending;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.gitlab.alpertoy.lending.dto.LoanApplicationDTO;
import io.gitlab.alpertoy.lending.dto.LoanRequestDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * User: alpertoy
 * Date: 15.02.2021
 * Time: 15:02
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase
public class LoanIntegrationTest {

    private static final String JOHN = "John";
    private static final Gson GSON = new Gson();

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int serverPort;

    @Test
    public void givenLoanRequestIsMadeLoanRequestDTOGetsCreated() throws Exception {
        String baseUrl = "http://localhost:" + serverPort + "/loan/";
        HttpHeaders httpHeaders = getHttpHeaders();

        HttpEntity<LoanRequestDTO> request = new HttpEntity<>(new LoanRequestDTO(50, 10, 10), httpHeaders);

        restTemplate.postForEntity(baseUrl + "/request", request, String.class);

        ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/requests", HttpMethod.GET,
                new HttpEntity<>(null, getHttpHeaders()), String.class);

        List<LoanApplicationDTO> loanApplicationDTOS = GSON.fromJson(response.getBody(), new TypeToken<List<LoanApplicationDTO>>(){}.getType());

        assertEquals(1, loanApplicationDTOS.size());
        assertEquals(loanApplicationDTOS.get(0).getBorrower().getUsername(), JOHN);

    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, JOHN);
        return httpHeaders;
    }
}
