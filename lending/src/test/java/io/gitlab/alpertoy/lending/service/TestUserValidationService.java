package io.gitlab.alpertoy.lending.service;

import io.gitlab.alpertoy.lending.entity.User;
import io.gitlab.alpertoy.lending.exception.UserNotFoundException;
import io.gitlab.alpertoy.lending.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * User: alpertoy
 * Date: 15.02.2021
 * Time: 18:02
 */

@Profile("test")
@Primary
@Component
public class TestUserValidationService implements TokenValidationService{

    private UserRepository userRepository;

    @Autowired
    public TestUserValidationService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User validateTokenAndGetUser(String token) {
        return userRepository.findById(token).orElseThrow(() -> new UserNotFoundException(token));
    }
}
