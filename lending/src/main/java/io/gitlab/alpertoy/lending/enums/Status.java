package io.gitlab.alpertoy.lending.enums;

/**
 * User: alpertoy
 * Date: 15.02.2021
 * Time: 12:23
 */
public enum Status {
    ONGOING, COMPLETED
}
