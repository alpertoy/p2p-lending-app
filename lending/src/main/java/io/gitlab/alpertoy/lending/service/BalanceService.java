package io.gitlab.alpertoy.lending.service;

import io.gitlab.alpertoy.lending.entity.Money;
import io.gitlab.alpertoy.lending.entity.User;
import io.gitlab.alpertoy.lending.exception.UserNotFoundException;
import io.gitlab.alpertoy.lending.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: alpertoy
 * Date: 14.02.2021
 * Time: 18:26
 */

@Service
public class BalanceService {

    private final UserRepository userRepository;

    @Autowired
    public BalanceService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public void topUpBalance(Money money, String authToken) {
        User user = findUser(authToken);
        user.topUp(money);
    }

    @Transactional
    public void withdrawFromBalance(Money money, String authToken) {
        User user = findUser(authToken);
        user.withDraw(money);
    }

    private User findUser(String authToken) {
        return userRepository.findById(authToken)
                .orElseThrow(() -> new UserNotFoundException(authToken));
    }
}
