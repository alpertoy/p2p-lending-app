package io.gitlab.alpertoy.lending.controller;

import io.gitlab.alpertoy.lending.dto.LoanRepaymentRequest;
import io.gitlab.alpertoy.lending.dto.LoanRequestDTO;
import io.gitlab.alpertoy.lending.entity.Loan;
import io.gitlab.alpertoy.lending.entity.LoanRequest;
import io.gitlab.alpertoy.lending.entity.User;
import io.gitlab.alpertoy.lending.enums.Status;
import io.gitlab.alpertoy.lending.repository.LoanRequestRepository;
import io.gitlab.alpertoy.lending.repository.UserRepository;
import io.gitlab.alpertoy.lending.adapter.LoanRequestAdapter;
import io.gitlab.alpertoy.lending.service.LoanService;
import io.gitlab.alpertoy.lending.service.TokenValidationService;
import io.gitlab.alpertoy.lending.service.TokenValidationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * User: alpertoy
 * Date: 7.02.2021
 * Time: 19:50
 */

@RestController
public class LoanController {

    private final LoanRequestRepository loanRequestRepository;

    private final UserRepository userRepository;

    private final LoanRequestAdapter loanRequestAdapter;

    private final LoanService loanService;

    private final TokenValidationService tokenValidationService;

    @Autowired
    public LoanController(LoanRequestRepository loanRequestRepository, UserRepository userRepository,
                          LoanRequestAdapter loanRequestAdapter, LoanService loanService, TokenValidationService tokenValidationService) {
        this.loanRequestRepository = loanRequestRepository;
        this.userRepository = userRepository;
        this.loanRequestAdapter = loanRequestAdapter;
        this.loanService = loanService;
        this.tokenValidationService = tokenValidationService;
    }

    @PostMapping(value = "/loan/request")
    public void requestLoan(@RequestBody LoanRequestDTO loanRequestDTO, HttpServletRequest request) {
        User borrower = tokenValidationService.validateTokenAndGetUser(request.getHeader(HttpHeaders.AUTHORIZATION));
        loanRequestRepository.save(loanRequestAdapter.transform(loanRequestDTO, borrower));
    }

    @GetMapping(value = "/loan/requests")
    public List<LoanRequest> findAllLoanRequests(HttpServletRequest request) {
        tokenValidationService.validateTokenAndGetUser(request.getHeader(HttpHeaders.AUTHORIZATION));
        return loanRequestRepository.findAllByStatusEquals(Status.ONGOING);
    }

    @GetMapping(value = "/loan/{status}/borrowed")
    public List<Loan> findBorrowedLoans(@RequestHeader String authorization, @PathVariable Status status) {
        User borrower = tokenValidationService.validateTokenAndGetUser(authorization);
        return loanService.findAllBorrowedLoans(borrower, status);
    }

    @GetMapping(value = "/loan/{status}/lent")
    public List<Loan> findLentLoans(@RequestHeader String authorization, @PathVariable Status status) {
        User lender = tokenValidationService.validateTokenAndGetUser(authorization);
        return loanService.findAllLentLoans(lender, status);
    }

    @PostMapping(value = "/loan/repay")
    public void repayLoan(@RequestBody LoanRepaymentRequest request, @RequestHeader String authorization) {
        User borrower = tokenValidationService.validateTokenAndGetUser(authorization);
        loanService.repayLoan(request.getAmount(), request.getLoanId(), borrower);
    }

    @PostMapping(value = "/loan/accept/{loanRequestId}")
    public void acceptLoan(@PathVariable String loanRequestId, HttpServletRequest request) {
        User lender = tokenValidationService.validateTokenAndGetUser(request.getHeader(HttpHeaders.AUTHORIZATION));
        loanService.acceptLoan(Long.parseLong(loanRequestId), lender.getUsername());
    }

    @GetMapping(value = "/loans")
    public List<Loan> getLoans(HttpServletRequest request) {
        tokenValidationService.validateTokenAndGetUser(request.getHeader(HttpHeaders.AUTHORIZATION));
        return loanService.getLoans();
    }
}
