package io.gitlab.alpertoy.lending.entity;

import io.gitlab.alpertoy.lending.enums.Currency;
import io.gitlab.alpertoy.lending.enums.Status;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Duration;
import java.util.Objects;

/**
 * User: alpertoy
 * Date: 7.02.2021
 * Time: 15:27
 */

@Entity
public class LoanRequest {

    @Id
    private long id;
    private int amount;
    @ManyToOne
    private User borrower;
    private int repaymentTermInDays;
    private double interestRate;
    private Status status;

    public LoanRequest() {

    }

    public LoanRequest(int amount, User borrower, int repaymentTermInDays, double interestRate) {
        this.amount = amount;
        this.borrower = borrower;
        this.repaymentTermInDays = repaymentTermInDays;
        this.interestRate = interestRate;
        this.status = Status.ONGOING;
    }

    public Loan acceptLoanApplication(User lender) {
        lender.withDraw(getAmount());
        borrower.topUp(getAmount());
        status = Status.COMPLETED;
        return new Loan(lender, this);
    }

    public Money getAmount() {
        return new Money(amount, Currency.USD);
    }

    public User getBorrower() {
        return borrower;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setBorrower(User borrower) {
        this.borrower = borrower;
    }

    public int getRepaymentTermInDays() {
        return repaymentTermInDays;
    }

    public void setRepaymentTermInDays(int repaymentTermInDays) {
        this.repaymentTermInDays = repaymentTermInDays;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoanRequest that = (LoanRequest) o;
        return id == that.id && amount == that.amount && repaymentTermInDays == that.repaymentTermInDays && Double.compare(that.interestRate, interestRate) == 0 && Objects.equals(borrower, that.borrower) && status == that.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, amount, borrower, repaymentTermInDays, interestRate, status);
    }

    @Override
    public String toString() {
        return "LoanRequest{" +
                "id=" + id +
                ", amount=" + amount +
                ", borrower=" + borrower +
                ", repaymentTermInDays=" + repaymentTermInDays +
                ", interestRate=" + interestRate +
                ", status=" + status +
                '}';
    }
}
