package io.gitlab.alpertoy.lending.dto;

import io.gitlab.alpertoy.lending.entity.Money;
import io.gitlab.alpertoy.lending.enums.Currency;

import java.util.Objects;

/**
 * User: alpertoy
 * Date: 15.02.2021
 * Time: 02:34
 */
public class LoanRepaymentRequest {

    private double amount;
    private long loanId;

    public LoanRepaymentRequest(double amount, long loanId) {
        this.amount = amount;
        this.loanId = loanId;
    }

    public Money getAmount() {
        return new Money(amount, Currency.USD);
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getLoanId() {
        return loanId;
    }

    public void setLoanId(long loanId) {
        this.loanId = loanId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoanRepaymentRequest that = (LoanRepaymentRequest) o;
        return Double.compare(that.amount, amount) == 0 && loanId == that.loanId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, loanId);
    }

    @Override
    public String toString() {
        return "LoanRepaymentRequest{" +
                "amount=" + amount +
                ", loanId=" + loanId +
                '}';
    }
}
