package io.gitlab.alpertoy.lending.dto;

import java.util.Objects;

/**
 * User: alpertoy
 * Date: 7.02.2021
 * Time: 21:35
 */
public class LoanRequestDTO {

    private int amount;
    private int daysToRepay;
    private double interestRate;

    public LoanRequestDTO() {
    }

    public LoanRequestDTO(int amount, int daysToRepay, double interestRate) {
        this.amount = amount;
        this.daysToRepay = daysToRepay;
        this.interestRate = interestRate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getDaysToRepay() {
        return daysToRepay;
    }

    public void setDaysToRepay(int daysToRepay) {
        this.daysToRepay = daysToRepay;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoanRequestDTO that = (LoanRequestDTO) o;
        return amount == that.amount && daysToRepay == that.daysToRepay && Double.compare(that.interestRate, interestRate) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, daysToRepay, interestRate);
    }

    @Override
    public String toString() {
        return "LoanRequestDTO{" +
                "amount=" + amount +
                ", daysToRepay=" + daysToRepay +
                ", interestRate=" + interestRate +
                '}';
    }
}
