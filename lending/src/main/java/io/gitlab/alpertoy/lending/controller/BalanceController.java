package io.gitlab.alpertoy.lending.controller;

import io.gitlab.alpertoy.lending.entity.Money;
import io.gitlab.alpertoy.lending.service.BalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * User: alpertoy
 * Date: 14.02.2021
 * Time: 18:29
 */

@RestController
@RequestMapping("/balance")
public class BalanceController {

    private final BalanceService balanceService;

    @Autowired
    public BalanceController(BalanceService balanceService) {
        this.balanceService = balanceService;
    }

    @PostMapping("/topup")
    public void topUp(@RequestBody Money money, @RequestHeader String authorization) {
        balanceService.topUpBalance(money, authorization);
    }

    @PostMapping("/withdraw")
    public void withdraw(@RequestBody Money money, @RequestHeader String authorization) {
        balanceService.withdrawFromBalance(money, authorization);
    }
}
