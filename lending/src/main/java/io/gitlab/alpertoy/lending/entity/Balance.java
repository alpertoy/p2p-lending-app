package io.gitlab.alpertoy.lending.entity;

import io.gitlab.alpertoy.lending.enums.Currency;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

/**
 * User: alpertoy
 * Date: 14.02.2021
 * Time: 14:12
 */

@Entity
public class Balance {

    @Id
    @GeneratedValue
    private long id;

    @ElementCollection
    @MapKeyClass(Currency.class)
    @OneToMany(targetEntity = Money.class, cascade = CascadeType.ALL)
    private Map<Currency, Money> moneyMap = new HashMap<>();

    public void topUp(Money money) {
        if (moneyMap.get(money.getCurrency()) == null) {
            moneyMap.put(money.getCurrency(), money);
        } else {
            moneyMap.put(money.getCurrency(), moneyMap.get(money.getCurrency()).add(money));
        }
    }

    public void withdraw(Money money) {
        Money moneyInBalance = moneyMap.get(money.getCurrency());
        if (moneyInBalance == null) {
            throw new IllegalArgumentException();
        } else {
            moneyMap.put(money.getCurrency(), moneyMap.get(money.getCurrency()).minus(money));
        }
    }

    public Map<Currency, Money> getMoneyMap() {
        return moneyMap;
    }

    public void setMoneyMap(Map<Currency, Money> moneyMap) {
        this.moneyMap = moneyMap;
    }
}
