package io.gitlab.alpertoy.lending.entity;

import io.gitlab.alpertoy.lending.enums.Currency;
import io.gitlab.alpertoy.lending.enums.Status;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

/**
 * User: alpertoy
 * Date: 8.02.2021
 * Time: 18:44
 */

@Entity
public class Loan {

    @Id
    @GeneratedValue
    private long id;
    @ManyToOne
    private User borrower;
    @ManyToOne
    private User lender;
    @OneToOne(cascade = CascadeType.ALL)
    private Money loanAmount;
    private double interestRate;
    private LocalDate dateLent;
    private LocalDate dateDue;
    @OneToOne(cascade = CascadeType.ALL)
    private Money amountRepayed;
    private Status status;

    public Loan() {
    }

    public Loan(User lender, LoanRequest loanRequest) {
        this.borrower = loanRequest.getBorrower();
        this.lender = lender;
        this.loanAmount = loanRequest.getAmount();
        this.interestRate = loanRequest.getInterestRate();
        this.dateLent = LocalDate.now();
        this.dateDue = LocalDate.now().plusDays(loanRequest.getRepaymentTermInDays());
        this.amountRepayed = Money.ZERO;
        this.status = Status.ONGOING;
    }

    public void repay(Money money) {
        borrower.withDraw(money);
        lender.topUp(money);
        amountRepayed = amountRepayed.add(money);

        if(getAmountOwed().equals(Money.ZERO)) {
            status = Status.COMPLETED;
        }
    }

    public Money getAmountOwed() {
        return loanAmount.times(1 + interestRate/100d).minus(amountRepayed);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getBorrower() {
        return borrower;
    }

    public void setBorrower(User borrower) {
        this.borrower = borrower;
    }

    public User getLender() {
        return lender;
    }

    public void setLender(User lender) {
        this.lender = lender;
    }

    public Money getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(Money amount) {
        this.loanAmount = amount;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public LocalDate getDateLent() {
        return dateLent;
    }

    public void setDateLent(LocalDate dateLent) {
        this.dateLent = dateLent;
    }

    public LocalDate getDateDue() {
        return dateDue;
    }

    public void setDateDue(LocalDate dateDue) {
        this.dateDue = dateDue;
    }

    public Money getAmountRepayed() {
        return amountRepayed;
    }

    public void setAmountRepayed(Money amountRepayed) {
        this.amountRepayed = amountRepayed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Loan loan = (Loan) o;
        return id == loan.id && Double.compare(loan.interestRate, interestRate) == 0 && Objects.equals(borrower, loan.borrower) && Objects.equals(lender, loan.lender) && Objects.equals(loanAmount, loan.loanAmount) && Objects.equals(dateLent, loan.dateLent) && Objects.equals(dateDue, loan.dateDue) && Objects.equals(amountRepayed, loan.amountRepayed) && status == loan.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, borrower, lender, loanAmount, interestRate, dateLent, dateDue, amountRepayed, status);
    }

    @Override
    public String toString() {
        return "Loan{" +
                "id=" + id +
                ", borrower=" + borrower +
                ", lender=" + lender +
                ", loanAmount=" + loanAmount +
                ", interestRate=" + interestRate +
                ", dateLent=" + dateLent +
                ", dateDue=" + dateDue +
                ", amountRepayed=" + amountRepayed +
                ", status=" + status +
                '}';
    }
}
