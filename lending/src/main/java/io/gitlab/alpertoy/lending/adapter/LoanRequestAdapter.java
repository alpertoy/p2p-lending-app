package io.gitlab.alpertoy.lending.adapter;

import io.gitlab.alpertoy.lending.dto.LoanRequestDTO;
import io.gitlab.alpertoy.lending.entity.LoanRequest;
import io.gitlab.alpertoy.lending.entity.User;
import io.gitlab.alpertoy.lending.exception.UserNotFoundException;
import io.gitlab.alpertoy.lending.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Optional;

/**
 * User: alpertoy
 * Date: 8.02.2021
 * Time: 01:52
 */

@Component
public class LoanRequestAdapter {

    private final UserRepository userRepository;

    @Autowired
    public LoanRequestAdapter(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public LoanRequest transform(LoanRequestDTO req, User borrower) {
        Optional<User> userOptional = userRepository.findById(borrower.getUsername());

        if(userOptional.isPresent()) {
            return new LoanRequest(req.getAmount(), userOptional.get(),
                    req.getDaysToRepay(), req.getInterestRate());
        } else {
            throw new UserNotFoundException(borrower.getUsername());
        }
    }
}
