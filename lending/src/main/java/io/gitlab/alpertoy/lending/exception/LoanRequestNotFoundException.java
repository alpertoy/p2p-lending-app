package io.gitlab.alpertoy.lending.exception;

/**
 * User: alpertoy
 * Date: 8.02.2021
 * Time: 19:25
 */
public class LoanRequestNotFoundException extends RuntimeException {

    public LoanRequestNotFoundException(long loanRequestId) {
        super("Loan application with id: " + loanRequestId + " was not found");
    }
}
