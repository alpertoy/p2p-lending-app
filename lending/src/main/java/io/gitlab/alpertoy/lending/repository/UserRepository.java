package io.gitlab.alpertoy.lending.repository;

import io.gitlab.alpertoy.lending.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * User: alpertoy
 * Date: 7.02.2021
 * Time: 22:12
 */
public interface UserRepository extends JpaRepository<User, String> {
}
