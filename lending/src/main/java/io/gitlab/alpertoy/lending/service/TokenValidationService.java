package io.gitlab.alpertoy.lending.service;

import io.gitlab.alpertoy.lending.entity.User;

/**
 * User: alpertoy
 * Date: 15.02.2021
 * Time: 17:26
 */
public interface TokenValidationService {

    User validateTokenAndGetUser(String token);
}
