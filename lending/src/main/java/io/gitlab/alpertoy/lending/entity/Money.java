package io.gitlab.alpertoy.lending.entity;

import io.gitlab.alpertoy.lending.enums.Currency;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

/**
 * User: alpertoy
 * Date: 14.02.2021
 * Time: 14:17
 */

@Entity
public class Money {

    public static final Money ZERO = new Money(0, Currency.USD);

    @Id
    @GeneratedValue
    private long id;

    private BigDecimal amount;
    private Currency currency;

    public Money() {
    }

    public Money(double amount, Currency currency) {
        this.amount = BigDecimal.valueOf(amount).setScale(2, RoundingMode.HALF_DOWN);
        this.currency = currency;
    }

    public Money times(double multiplier) {
        return new Money(amount.doubleValue() * multiplier, currency);
    }

    public Money add(Money money) {
        if (currency != money.getCurrency()) {
            throw new IllegalArgumentException();
        }
        return new Money(amount.doubleValue() + money.getAmount(), currency);
    }

    public Money minus(Money money) {
        if (currency != money.getCurrency() || amount.doubleValue() < money.getAmount()) {
            throw new IllegalArgumentException();
        }
        return new Money(amount.doubleValue() - money.getAmount(), currency);
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount.doubleValue();
    }

    public void setAmount(double amount) {
        this.amount = BigDecimal.valueOf(amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Money money = (Money) o;
        return id == money.id && Objects.equals(amount, money.amount) && currency == money.currency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, amount, currency);
    }

    @Override
    public String toString() {
        return "Money{" +
                "id=" + id +
                ", amount=" + amount +
                ", currency=" + currency +
                '}';
    }
}
