package io.gitlab.alpertoy.lending.service;

import io.gitlab.alpertoy.lending.entity.Loan;
import io.gitlab.alpertoy.lending.entity.LoanRequest;
import io.gitlab.alpertoy.lending.entity.Money;
import io.gitlab.alpertoy.lending.entity.User;
import io.gitlab.alpertoy.lending.enums.Currency;
import io.gitlab.alpertoy.lending.enums.Status;
import io.gitlab.alpertoy.lending.exception.LoanNotFoundException;
import io.gitlab.alpertoy.lending.exception.LoanRequestNotFoundException;
import io.gitlab.alpertoy.lending.exception.UserNotFoundException;
import io.gitlab.alpertoy.lending.repository.LoanRepository;
import io.gitlab.alpertoy.lending.repository.LoanRequestRepository;
import io.gitlab.alpertoy.lending.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: alpertoy
 * Date: 8.02.2021
 * Time: 19:13
 */

@Service
public class LoanService {

    private final LoanRequestRepository loanRequestRepository;
    private final UserRepository userRepository;
    private final LoanRepository loanRepository;

    @Autowired
    public LoanService(LoanRequestRepository loanRequestRepository, UserRepository userRepository, LoanRepository loanRepository) {
        this.loanRequestRepository = loanRequestRepository;
        this.userRepository = userRepository;
        this.loanRepository = loanRepository;
    }

    @Transactional
    public void repayLoan(Money amountToRepay, long loanId, User borrower) {
        Loan loan = loanRepository.findOneByIdAndBorrower(loanId, borrower)
                .orElseThrow(LoanNotFoundException::new);

        Money actualPaidAmount = amountToRepay.getAmount() > loan.getAmountOwed().getAmount() ?
                loan.getAmountOwed() : amountToRepay;

        loan.repay(actualPaidAmount);
    }

    @Transactional
    public void acceptLoan(long loanRequestId, String lenderUsername) {
        User lender = findUser(lenderUsername);
        LoanRequest loanRequest = findLoanApplication(loanRequestId);
        loanRepository.save(loanRequest.acceptLoanApplication(lender));
    }

    public List<Loan> findAllBorrowedLoans(User borrower, Status status) {
        return loanRepository.findAllByBorrowerAndStatus(borrower, status);
    }

    public List<Loan> findAllLentLoans(User lender, Status status) {
        return loanRepository.findAllByLenderAndStatus(lender, status);
    }

    private LoanRequest findLoanApplication(long loanRequestId) {
        return loanRequestRepository.findById(loanRequestId)
                .orElseThrow(() -> new LoanRequestNotFoundException(loanRequestId));
    }

    private User findUser(String lenderUsername) {
        return userRepository.findById(lenderUsername).orElseThrow(() -> new UserNotFoundException(lenderUsername));
    }

    public List<Loan> getLoans() {
        return loanRepository.findAll();
    }
}
