package io.gitlab.alpertoy.lending;

import io.gitlab.alpertoy.lending.entity.Balance;
import io.gitlab.alpertoy.lending.entity.User;
import io.gitlab.alpertoy.lending.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LendingApplication implements CommandLineRunner {

	@Autowired
	private UserRepository userRepository;

	public static void main(String[] args) {
		SpringApplication.run(LendingApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		userRepository.save(new User("John", "John", "D", 25, "Software Developer", new Balance()));
		userRepository.save(new User("Mark", "Mark", "C", 23, "Unemployed", new Balance()));
		userRepository.save(new User("Peter", "Peter", "E", 30, "Pilot", new Balance()));
	}
}
