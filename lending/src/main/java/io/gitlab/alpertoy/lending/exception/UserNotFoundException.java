package io.gitlab.alpertoy.lending.exception;

/**
 * User: alpertoy
 * Date: 8.02.2021
 * Time: 02:08
 */
public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String username) {
        super("User with id: " + username + " not found");
    }
}
