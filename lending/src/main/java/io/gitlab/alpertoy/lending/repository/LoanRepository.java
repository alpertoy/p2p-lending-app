package io.gitlab.alpertoy.lending.repository;

import io.gitlab.alpertoy.lending.entity.Loan;
import io.gitlab.alpertoy.lending.entity.User;
import io.gitlab.alpertoy.lending.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * User: alpertoy
 * Date: 8.02.2021
 * Time: 19:07
 */
public interface LoanRepository extends JpaRepository<Loan, Long> {

    List<Loan> findAllByBorrowerAndStatus(User borrower, Status status);

    List<Loan> findAllByLenderAndStatus(User lender, Status status);

    Optional<Loan> findOneByIdAndBorrower(Long id, User borrower);
}
