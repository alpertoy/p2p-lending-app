package io.gitlab.alpertoy.lending.service;

import io.gitlab.alpertoy.lending.entity.User;
import io.gitlab.alpertoy.lending.exception.UserNotFoundException;
import io.gitlab.alpertoy.lending.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * User: alpertoy
 * Date: 9.02.2021
 * Time: 19:54
 */

@Service
public class TokenValidationServiceImpl implements TokenValidationService {

    private final UserRepository userRepository;
    private final RestTemplate restTemplate = new RestTemplate();
    private final String securityContextBaseUrl;

    @Autowired
    public TokenValidationServiceImpl(UserRepository userRepository, @Value("${security.baseurl}") String securityContextBaseUrl) {
        this.userRepository = userRepository;
        this.securityContextBaseUrl = securityContextBaseUrl;
    }

    public User validateTokenAndGetUser(String token) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, token);
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        ResponseEntity<String> response = restTemplate
                .exchange(securityContextBaseUrl + "/user/validate", HttpMethod.POST, httpEntity, String.class);

        if (response.getStatusCode().equals(HttpStatus.OK)) {
            return userRepository.findById(response.getBody())
                    .orElseThrow(() -> new UserNotFoundException(response.getBody()));
        }

        throw new RuntimeException("Invalid token");
     }
}
