package io.gitlab.alpertoy.lending.repository;

import io.gitlab.alpertoy.lending.entity.LoanRequest;
import io.gitlab.alpertoy.lending.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * User: alpertoy
 * Date: 7.02.2021
 * Time: 21:25
 */
public interface LoanRequestRepository extends JpaRepository<LoanRequest, Long> {

    List<LoanRequest> findAllByStatusEquals(Status status);
}
