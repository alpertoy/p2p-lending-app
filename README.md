# Lending Application

Lending application is where users lend money to each other.
Based on microservices that consists of three services Lending, Profile and Security connected via RabbitMQ

## Usage

Make sure you are running RabbitMQ on your machine before testing

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/2c712ee859d8877afe2f)
